FROM openjdk:7-alpine

WORKDIR /app

COPY . .
RUN chmod +x run.sh

EXPOSE 8080
ENTRYPOINT ["run.sh"] 
